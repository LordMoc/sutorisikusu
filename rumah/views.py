from django.shortcuts import render, redirect
from .models import Status
from .forms import FormTamvan

# Create your views here.
def StupidFunction(request):
    form = FormTamvan()
    pewaktu = Status.objects.order_by("tanggal", "waktu")
    response = pewaktu
    if request.method == "POST":
        form=FormTamvan(request.POST)
        if form.is_valid(): 
            form.save()
            return redirect('rumah')
    return render(request, 'index.html', {'form' : form, 'response' : response})