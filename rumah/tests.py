from django.urls import resolve
from selenium import webdriver
from django.test import TestCase,LiveServerTestCase,Client
from selenium.webdriver.common.keys import Keys
from .views import StupidFunction
from .models import Status
from .forms import FormTamvan
from django.apps import apps
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class TestTamvan(TestCase):

    def test_url_exist(self):
        ada = Client().get('')
        self.assertEqual(ada.status_code, 200)
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)#test existing url
        response = Client().get('/admin')
        self.assertNotEqual(response.status_code, 200)#test url that doesn't exist
        response = Client().get('/rumah')
        self.assertNotEqual(response.status_code, 200)
    def test_function(self):
        foundFunc = resolve('/')
        self.assertEqual(foundFunc.func, StupidFunction)
    def test_model(self):
        status = Status.objects.create(status='ABCDEFGH')
        self.assertIsInstance(status, Status)#test object creation
    def test_form(self):
        form = FormTamvan(data={'status':''})#test empty
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ["This field is required."])
        form = FormTamvan(data={'status':(200*'0123456789')})#test over limit
        self.assertFalse(form.is_valid())
        form = FormTamvan(data={'status':'ABCDEFGH'})#test valid
        self.assertTrue(form.is_valid())
    def test_sutorisikusu_post_success_and_render_the_result(self):
        response_post = Client().post('/', {'status':'Sutori Ka Peue!'})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn('Sutori Ka Peue', html_response)
    
    def test_sutorisikusu_post_failed_and_render_the_result(self):
        response_post = Client().post('/', {'status':100*'lima'})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(100*'lima', html_response)

class AccountTestCase(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

        super(AccountTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(AccountTestCase, self).tearDown()

    def test_register(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('guegaktau')
        status.send_keys('Status Input testing')
        submit.send_keys(Keys.RETURN)
        assert 'Status Input testing' in selenium.page_source

        
     
