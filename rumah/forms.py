from . import models
from django import forms
import datetime

class FormTamvan(forms.ModelForm):
    status = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "statusfield full",
        "required" : True,
        "placeholder" : "STATUS",
    }))

    class Meta:
        model = models.Status
        fields = ["status"]