from django.db import models
import datetime
from django.utils import timezone

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=300)
    tanggal = models.DateField(auto_now_add=True)
    waktu = models.TimeField(auto_now_add=True)

    def __str__(self):
        return self.status
